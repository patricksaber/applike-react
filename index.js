/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/components/HomePage';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
