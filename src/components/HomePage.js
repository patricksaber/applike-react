import React, {Component} from 'react';
import {render} from 'react-dom';
import {Platform, StyleSheet, Text, Button, View} from 'react-native';
import { createStackNavigator } from 'react-navigation';
 

export default class HomePage extends Component{
  
  constructor(props){
    super(props);
    this.state = {
      species : [],
      fetched : false,
      loading : false,
    };
  }
  componentWillMount(){
    this.setState({
      loading : true
    });
    fetch('http://pokeapi.co/api/v2/pokemon?limit=151').then(res=>res.json())
    .then(response=>{
      this.setState({
        species : response.results,
        loading : true,
        fetched : true
      });
    });
  }

  getPokemonData(){
    //GET request 
    fetch('http://pokeapi.co/api/v2/pokemon/', {
        method: 'GET'
    
    })
    .then((response) => response.json())
    //If response is in json then in success
    .then((responseJson) => {
        var count = responseJson.count
        console.log("Pokemon count"+count);
        alert(JSON.stringify(responseJson));
        console.log(responseJson);
    })
    //If response is not in json then in error
    .catch((error) => {
        //Error 
        alert(JSON.stringify(error));
        console.error(error);
    });
  }
 
  
  render(){
    const {fetched, loading, species} = this.state;
    let content ;
    if(fetched){
      content = <div className="pokemon--species--list">{species.map((pokemon,index)=><Pokemon key={pokemon.name} id={index+1} pokemon={pokemon}/>)}</div>;
    }else if(loading && !fetched){
        content = <p> Loading ...</p>;
    }
    else{
      content = <div/>;
    }
    return  <div>
      {content}
    </div>;
  }
}

